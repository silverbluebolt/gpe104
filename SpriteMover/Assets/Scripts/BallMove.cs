﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour {

	private Transform move; //create a SpriteRenderer object

	// Use this for initialization
	void Start () {
		move = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		//move the beachball in a squarr
		if (move.position.x < 4 && move.position.y == 4) {
			//move the beachball right from the upper left
			move.position = move.position + (Vector3.right * 0.5f);
		} else if (move.position.x == 4 && move.position.y > -4) {
			//move the beachball down from the upper right
			move.position = move.position + (Vector3.down * 0.5f);
		} else if (move.position.x > -4 && move.position.y == -4) {
			//move the beachball left from the lower right
			move.position = move.position + (Vector3.left * 0.5f);
		} else if (move.position.x == -4 && move.position.y < 4) {
			//move the beachball up from the lower left
			move.position = move.position + (Vector3.up * 0.5f);
		}
	}
}
