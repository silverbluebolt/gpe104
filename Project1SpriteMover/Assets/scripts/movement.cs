﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{

    private Transform tf;
    public float speed = 0.1f;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftArrow)) //northwest
        {
            Vector3 direction = new Vector3(-1, 1, 0); //create a vector that points to the correct direction
            tf.right = direction; //point the sprite in the correct direction
            if (Input.GetKeyDown(KeyCode.LeftShift)) //when left shift is pressed, move only one unit in this direction
            {
                direction = direction.normalized; //make vector one unit long
                tf.position += direction; //change position by adding the movement vector
            }
            else if (!Input.GetKey(KeyCode.LeftShift)) //don't run if left shift is still pressed
            {
                direction = direction * speed; //scale the direction by the speed
                tf.position += direction; //change position by adding the movement vector
            }

        }
        else if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.RightArrow)) //northeast
        {
            Vector3 direction = new Vector3(1, 1, 0); //create a vector that points to the correct direction
            tf.right = direction; //point the sprite in the correct direction
            if (Input.GetKeyDown(KeyCode.LeftShift)) //when left shift is pressed, move only one unit in this direction
            {
                direction = direction.normalized; //make vector one unit long
                tf.position += direction; //change position by adding the movement vector
            }
            else if (!Input.GetKey(KeyCode.LeftShift)) //don't run if left shift is still pressed
            {
                direction = direction * speed; //scale the direction by the speed
                tf.position += direction; //change position by adding the movement vector
            }
        }
        else if (Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.RightArrow)) //southeast
        {
            Vector3 direction = new Vector3(1, -1, 0); //create a vector that points to the correct direction
            tf.right = direction; //point the sprite in the correct direction
            if (Input.GetKeyDown(KeyCode.LeftShift)) //when left shift is pressed, move only one unit in this direction
            {
                direction = direction.normalized; //make vector one unit long
                tf.position += direction; //change position by adding the movement vector
            }
            else if (!Input.GetKey(KeyCode.LeftShift)) //don't run if left shift is still pressed
            {
                direction = direction * speed; //scale the direction by the speed
                tf.position += direction; //change position by adding the movement vector
            }
        }
        else if (Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.LeftArrow)) //southwest
        {
            Vector3 direction = new Vector3(-1, -1, 0); //create a vector that points to the correct direction
            tf.right = direction; //point the sprite in the correct direction
            if (Input.GetKeyDown(KeyCode.LeftShift)) //when left shift is pressed, move only one unit in this direction
            {
                direction = direction.normalized; //make vector one unit long
                tf.position += direction; //change position by adding the movement vector
            }
            else if (!Input.GetKey(KeyCode.LeftShift)) //don't run if left shift is still pressed
            {
                direction = direction * speed; //scale the direction by the speed
                tf.position += direction; //change position by adding the movement vector
            }
        }
        else if (Input.GetKey(KeyCode.UpArrow)) //north
        {
            Vector3 direction = new Vector3(0, 1, 0); //create a vector that points to the correct direction
            tf.right = direction; //point the sprite in the correct direction
            if (Input.GetKeyDown(KeyCode.LeftShift)) //when left shift is pressed, move only one unit in this direction
            {
                tf.position += direction; //change position by adding the movement vector
            }
            else if (!Input.GetKey(KeyCode.LeftShift)) //don't run if left shift is still pressed
            {
                direction = direction * speed; //scale the direction by the speed
                tf.position += direction; //change position by adding the movement vector
            }
        }
        else if (Input.GetKey(KeyCode.LeftArrow)) //west
        {
            Vector3 direction = new Vector3(-1, 0, 0); //create a vector that points to the correct direction
            tf.right = direction; //point the sprite in the correct direction
            if (Input.GetKeyDown(KeyCode.LeftShift)) //when left shift is pressed, move only one unit in this direction
            {
                tf.position += direction; //change position by adding the movement vector
            }
            else if (!Input.GetKey(KeyCode.LeftShift)) //don't run if left shift is still pressed
            {
                direction = direction * speed; //scale the direction by the speed
                tf.position += direction; //change position by adding the movement vector
            }
        }
        else if (Input.GetKey(KeyCode.RightArrow)) //east
        {
            Vector3 direction = new Vector3(1, 0, 0); //create a vector that points to the correct direction
            tf.right = direction; //point the sprite in the correct direction
            if (Input.GetKeyDown(KeyCode.LeftShift)) //when left shift is pressed, move only one unit in this direction
            {
                tf.position += direction; //change position by adding the movement vector
            }
            else if (!Input.GetKey(KeyCode.LeftShift)) //don't run if left shift is still pressed
            {
                direction = direction * speed; //scale the direction by the speed
                tf.position += direction; //change position by adding the movement vector
            }
        }
        else if (Input.GetKey(KeyCode.DownArrow)) //south
        {
            Vector3 direction = new Vector3(0, -1, 0); //create a vector that points to the correct direction
            tf.right = direction; //point the sprite in the correct direction
            if (Input.GetKeyDown(KeyCode.LeftShift)) //when left shift is pressed, move only one unit in this direction
            {
                tf.position += direction; //change position by adding the movement vector
            }
            else if (!Input.GetKey(KeyCode.LeftShift)) //don't run if left shift is still pressed
            {
                direction = direction * speed; //scale the direction by the speed
                tf.position += direction; //change position by adding the movement vector
            }
        }

        if (Input.GetKeyDown(KeyCode.W)) //press 'w' to increase the speed
        {
            speed += 0.1f;
        }
        else if (Input.GetKeyDown(KeyCode.S)) //press 's' to decrease the speed
        {
            speed -= 0.1f;
        }

        if (Input.GetKeyDown(KeyCode.Space)) //return to the center when the space bar is pressed
        {
            Vector3 center = new Vector3(0, 0, 0); //create a zero vector
            tf.position = center; //set position to zero
        }
    }
}
