﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enabler : MonoBehaviour {

    public GameObject boats; //create variables to hold the sprite gameObjects and the movement script
    public GameObject ship;
    private movement move;

	// Use this for initialization
	void Start () {
        //boats and ship variables were set in inspector
        move = GetComponent<movement>(); //get the movement script connected to this gameObject
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.P)) //when p is pressed, toggle which sprite is active 
        {
            if (boats.activeInHierarchy == true) //if the boat sprite is active
            {
                boats.SetActive(false); //deactivate the boat sprite
                ship.SetActive(true); //activate the spaceship sprite
            } else  //otherwise
            {
                boats.SetActive(true); //activate the ship sprite 
                ship.SetActive(false); //deactivate the spaceship sprite
            }
        }

        if (Input.GetKeyDown(KeyCode.O)) //when the "o" key is pressed
        {
            move.enabled = !move.enabled; //set the enabled value to its opposite
        }
	}
}
